const fs = require('fs');

if(fs.existsSync('data/plants.txt') && !fs.existsSync('data/plants.json')) {
	const plants = fs.readFileSync('data/plants.txt', 'utf-8').trim().split('\n');
	const jsPlants = JSON.stringify(plants.map(name=>({name})), null, 2);
	fs.writeFileSync('data/plants.json', jsPlants)
	fs.unlinkSync('data/plants.txt')
}
