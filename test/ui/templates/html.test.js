const page = require('../../../src/ui/templates/html');

test('Generates html template', () => {
  expect(page({ title: 'Title' }, 'Content').toString()).toMatchSnapshot();
});
