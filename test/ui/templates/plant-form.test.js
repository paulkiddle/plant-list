const form = require('../../../src/ui/templates/plant-form.js');

test('Form renders', () => {
  expect(form({}, { plants: [
    { id: 1, name: 'A' }, { id: 2, name: 'B', repotted: '2020-12-01' }] }).toString()).toMatchSnapshot();
});

test('Form parses', () => {
  expect(form.parse('delete=A&delete=B&new=C&repot=D')).toMatchSnapshot();
});

test('Form parses multipart', () => {
  const req = {
    url: '/?id=1',
    headers: {
      'content-type': 'multipart/form-data; boundary=-------etc'
    },
    pipe: jest.fn()
  }

  expect(form.parse(req, true)).toBeInstanceOf(Promise);

  expect(req.pipe).toHaveBeenCalled();
});
