const redirect = require('../../src/ui/redirect');

test('Redirects response', () => {
	const res = {
		setHeader: jest.fn(),
	}

	const rtn = redirect(res, '/home');
	expect(rtn).toBe(res);
	expect(res.setHeader).toHaveBeenCalledWith('Location', '/home');
	expect(res.statusCode).toBe(303);
});
