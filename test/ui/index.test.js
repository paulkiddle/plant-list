jest.mock('../../src/ui/templates/plant-form', ()=>(...args)=>JSON.stringify({name:'plant form', args}, null, 2));

const getRouter = require('../../src/ui/index');
const Router = getRouter.Router;

test('Gets router', () => {
	const router = getRouter({});
	expect(router).toBeInstanceOf(Function);
});


test('Routes plant list', async () => {
	const request = {
		url: '/'
	};
	const plants = ['Plant list'];

	const router = new Router({
		getPlants(){
			return plants
		}
	});

	expect(await router.route({request})).toMatchSnapshot();
})
