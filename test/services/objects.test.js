const fs = require('fs');
const objectStore = require('../../src/services/objects');
const objects = objectStore();

test('setsImage', () => {
	const stream = {
		pipe: jest.fn(),
		on: jest.fn(function(event, callback){
			this.end = callback;
		})
	};

	const writeStream = 'Write stream';
	fs.createWriteStream = jest.fn(() => writeStream);
	fs.writeFileSync = jest.fn();

	objects.setImage(1, stream);

	expect(fs.createWriteStream).toHaveBeenCalledWith('/tmp/img-1.jpg');
	expect(stream.pipe).toHaveBeenCalledWith(writeStream);
	expect(fs.writeFileSync).toHaveBeenCalledWith('/tmp/img-1.mime', 'image/jpeg');
})

test('gets Image', () => {
	const readStream = 'Read stream';
	fs.createReadStream = jest.fn(() => readStream);
	const rfs = fs.readFileSync;
	fs.readFileSync = jest.fn(() => 'image/png');

	expect(objects.getImage(1)).toMatchSnapshot();
	expect(fs.createReadStream).toHaveBeenCalledWith('/tmp/img-1.jpg');
	expect(fs.readFileSync).toHaveBeenCalledWith('/tmp/img-1.mime', 'utf-8');

	fs.readFileSync = rfs;
});
