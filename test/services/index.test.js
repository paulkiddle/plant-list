const { backend } = require('../../src/services');

const plants = {
	get: jest.fn(),
	repot: jest.fn(),
	add: jest.fn(),
	delete: jest.fn()
}

test('Backend fns', async ()=>{
	const be = await backend({plants});

	be.getPlants();
	expect(plants.get).toHaveBeenCalled();

	be.repotPlant();
	expect(plants.repot).toHaveBeenCalled();

	be.addPlant();
	expect(plants.add).toHaveBeenCalled();

	be.rmPlant();
	expect(plants.delete).toHaveBeenCalled();
})
