const fs = require('fs');
const plants = require('../../src/services/plants');

test('Writes and reads plants', async () => {
	const p = await plants();
	await p.add('One');
	await p.add('Two');
	expect(await p.get()).toMatchSnapshot();

	await p.delete(1);

	expect(await p.get()).toMatchSnapshot();

	await p.repot(2);

	const d = new Date;

	expect((await p.get())[0].repotted).toEqual([d.getFullYear(), d.getMonth()+1, d.getDate()].map(n=>String(n).padStart(2, '0')).join('-'));
});
