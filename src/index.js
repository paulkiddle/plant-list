const ui = require('./ui');
const services = require('./services');

const app = services.then(ui);

module.exports = app;
