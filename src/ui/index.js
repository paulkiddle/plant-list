const { Response } = require('servie');
const { createHandler } = require('servie-http');
const es = require('expressive-switch');
const html = require('encode-html-template-tag')
const page = require('./templates/html');
const form = require('./templates/plant-form');
const { redirect } = require('servie-redirect');
const { parse } = require('get-body');
const contentType = require('content-type');
const getBody = (req, form) => parse(req, req.headers, { formParse: form.parse });

class Router {
	#services;

	constructor(backend){
		this.#services = backend;
	}

	static router(backend){
		const r = new Router(backend);
		return createHandler(r.route.bind(r));
	}

	async route(req) {
		const component = await this.getPage(req.request);

		if(component instanceof Response) {
			return component;
		}

		const r = new Response(
			page({ colour: '#99cc00', title: 'Plant list' }, component).render(),
			{
				headers: {
					'content-type': 'text/html'
				}
			}
		);

		return r;
	}

	async getPage(req) {
		let args;
		const [subject, match] = es((subject, value) => {
			if(value instanceof RegExp) {
				return args = subject.match(value);
			}

			return subject === value ? [value] : null;
		});

		switch(subject(req.url.split('?')[0])) {
			case match('/'):
				return this.getPlantList(req);
			case match(/^\/image\/([0-9]+)/):
				const { stream, mimeType } = this.#services.getImage(args[1]);
				return new Response(
					stream,
					{
						headers: {
							'content-type': mimeType
						}
					}
				)
			default:
				return html`Page not found`;
		}
	}

	async getPlantList(req) {
		if(req.method==='POST') {
			if(contentType.parse(req).type === 'multipart/form-data') {
				const body = await form.parse(req, true);

				await this.#services.setPlantImage(body.id, body.file, body.mimetype);
				return redirect(req, '/');
			}

			const body = await getBody(req, form);

			for(const name of body.delete) {
				this.#services.rmPlant(name);
			}
			for(const name of body.repot) {
				this.#services.repotPlant(name);
			}

			if(body.new) {
				this.#services.addPlant(body.new);
			}

			return redirect(req, '/');
		}

		const plants = await this.#services.getPlants();

		const plantForm = form({}, { plants });

		return plantForm;
	}
}

module.exports = Router.router;
module.exports.Router = Router;
