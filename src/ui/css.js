const html = require('encode-html-template-tag');

const css = (...args) => html`<style>${html(...args)}</style>`;

module.exports = css;
