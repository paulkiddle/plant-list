module.exports = function redirect(res, location, status=303) {
	res.statusCode = status;
	res.setHeader('Location', location);
	return res;
}
