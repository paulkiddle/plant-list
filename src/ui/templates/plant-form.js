const html = require('encode-html-template-tag');
const Busboy = require('busboy');
const css = require('../css');

const styles = css`
.PlantForm{
	width: 100%;
}

.PlantForm__textarea{
	width:100%;
	height:90%;
}

.PlantForm > form{
	display: flex;
	flex-wrap: wrap;
	margin: -15px;
}

.PlantForm > form > *{
	margin: 15px;
}

.plantItem > h2 {
	margin-top: 0;
}

.plantItem {
	background: #F1F1F1;
	padding: 15px;
}

.plantItem > img {
	display: block;
	max-width: 500px;
}
`;

const plants = 'plants';
const addNew = 'new';
const delPlant = 'delete';
const repot = 'repot';

const plantItem = ({id, name, repotted }) =>
	html`<div class="plantItem">
		<h2>${name}</h2>
		${repotted ? html`Repotted: ${repotted}` : ''}
		<button name="${repot}" value="${id}">repot</button>
		<button name="${delPlant}" value="${id}">x</button>
		<img src="/image/${id}">
		<input type="file" name="image" />
		<button name="upload" formaction="?id=${id}" formenctype="multipart/form-data">Upload</button>
	</div>`

const form = ({}, values) => html`
${styles}
<div class="PlantForm">
	<form method="post">
		<input name="${addNew}"><button>Add</button>
	</form>
	<form method="post">
		${values.plants.map(plantItem)}
		<!--textarea class="PlantForm__textarea" name="${plants}">${values.plants.join('\n')}</textarea><br-->
	</form>
</div>`;

form.parse = (body, multipart) => {
	if(multipart) return parseMultipart(body);

	const params = new URLSearchParams(body);
	return {
		//plants: params.get(plants).split('\n'),
		delete: params.getAll(delPlant),
		repot: params.getAll(repot),
		new: params.get(addNew)
	};
}

function parseMultipart(req) {
	const s = new URLSearchParams(req.url.split('?')[1] || '');
	const id = s.get('id');
	const busboy = new Busboy({ headers: req.headers });
	const p = new Promise(resolve => {
		busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
			if(fieldname === 'image' && filename) {
				resolve({
					id,
					filename,
					file,
					mimetype
				});
			} else {
				file.resume();
			}
		});
	});

	req.pipe(busboy);
	return p;
}

module.exports = form;
