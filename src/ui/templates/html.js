const html = require('encode-html-template-tag');
const css = require('../css');

const style = css`
html, body {
	margin: 0;
	padding: 0;
	height: 100%;
}
* {
	box-sizing: border-box;
}
body {
	display: flex;
	flex-direction: column;
	font-family: Arial, sans-serif;
}

main.content {
	flex-grow: 1;
	margin-top: 30px;
}
main h1, main h2 {
	color: var(--theme-colour);
}
header {
	background: var(--theme-colour);
	color: white;
	min-height: 170px;
	display: flex;
}
.content {
	width: 100%;
	max-width: 1200px;
	margin: 0 auto;
	padding: 10px;
}
footer {
	background: #F1F1F1;
	color: var(--theme-color);
}
header h1 {
	font-weight: normal;
	margin: 32px 0 0 0;
}
main h1 {
	font-weight: normal;
	letter-spacing: -0.05em;
	margin-top: 0;
}
`;

const page = ({title, colour, header}, body) => html`<!doctype HTML><html style="--theme-colour: ${colour || 'black'}">
<head><meta name="viewport" content="width=device-width, initial-scale=1">
${style}
<title>${title}</title></head>
<body>
	<header>
		<div class="content">
		  <h1>${title}</h1>
		</div>
	</header>
	<main class="content">
		${body}
	</main>
	<footer>
		<div class="content">`;

module.exports = page;
