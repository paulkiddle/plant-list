const sqlite3 = require('sqlite3');
const { open } = require('sqlite');
const sql = require('sql-template-tag').default;

const store = async (storageFile = ':memory:') => {
	const db = await open({
		filename: storageFile,
		driver: sqlite3.Database
	});

	await db.migrate({ migrationsPath: 'src/services/plants/migrations' });

	return {
		async get(){
			return await db.all(sql`SELECT *, rowid as id FROM plants`);
		},
		async add(plant){
			const q = sql`INSERT INTO plants (name, repotted) VALUES(${plant}, '')`;
			await db.run(q);
		},
		async delete(id) {
			await db.run(sql`DELETE FROM plants WHERE rowid = ${id}`);
		},
		async repot(id) {
			await db.run(sql`UPDATE plants SET repotted=date() WHERE rowid = ${id}`);
		}
	}
}

module.exports = store;
