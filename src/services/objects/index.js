const fs = require('fs');

const tryRead = (file, fallback) => {
	try {
		return fs.readFileSync(`${file}.mime`, 'utf-8')
	} catch(e) {
		console.log(e);
		return fallback;
	}
}

const store = (imgDir = '/tmp') => {
	return {
		async setImage(id, stream, mimeType = 'image/jpeg'){
			if(mimeType.indexOf('image') !== 0){
				throw new TypeError('Mime type should be image.');
			}
			const file = `${imgDir}/img-${id}`;
			stream.pipe(fs.createWriteStream(`${file}.jpg`));
			fs.writeFileSync(`${file}.mime`, mimeType);
			return new Promise(res=>stream.on('end', res));
		},
		getImage(id){
			const file = `${imgDir}/img-${id}`;
			return {
				mimeType: tryRead(file, 'image/jpeg'),
				stream: fs.createReadStream(`${file}.jpg`)
			}
		}
	}
}

module.exports = store;
