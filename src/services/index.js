const path = require('path');
const storage = require('./plants');
const objectStore = require('./objects')

const storageFile = path.resolve('./data/plants.sqlite3');
const plants = storage(storageFile);
const objects = objectStore('./data/files');

async function plantStorage({ plants, objects }) {
	plants = await plants;

	return {
		getPlants: ()=>plants.get(),
		addPlant: p=>plants.add(p),
		rmPlant: p=>plants.delete(p),
		repotPlant: p=>plants.repot(p),
		setPlantImage: (...args) => objects.setImage(...args),
		getImage: id => objects.getImage(id)
	}
}

module.exports = plantStorage({plants, objects});
module.exports.backend=plantStorage;
