# JavaScript application template

When developing a JavaScript application there are certain useful patterns and practices I keep coming back to over again. This is an attempt to collect them in one place to make it easier to get development up and running quickly.

Although I've called this a "template", it's more of a springboard. I've tried to minimise the ammount of code that needs to be removed and replaced, so that starting up is as quick as clone, install, and run.

## How to use

All you need is git and node. At minimum all you have to do is clone the repository, CD into it, and run `npm init`, although I like the following workflow:

For the initial setup:

- Create a directory for all git repos (mine's `~/repos`)
- Clone the template into a sub directory (e.g. `~/repos/template`)

Then, whenever you want to createa new project:

- CD to your repos directoy (`cd ~/repos`)
- Clone the template again under a new name (`git clone template new-project`)
- CD into your new project and run `npm init` and `npm install`
- Start coding!
